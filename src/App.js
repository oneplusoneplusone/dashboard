import React from 'react';
import { Document, Page } from 'react-pdf';
import { pdfjs } from 'react-pdf';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

function importAll(r) {
  return r.keys().map(r);
}

class Slideshow extends React.Component {
  constructor(props) {
    super(props);

    this.state = { seconds: 1 };
    this.pageNumber = 1;
    this.numPages = 1;
    this.delay = 15;

    this.pdfs = importAll(
      require.context("./pdfs", false, /\.(pdf)$/)
    );

    if (this.pdfs.length === 0) {
      console.log("No pdf found in pdfs dir, using default...");
      this.pdfs = importAll(
        require.context(".", false, /default\.(pdf)$/)
      );
    }

    this.pdfIndex = 0;

    console.log("Iterating through " + this.pdfs.length + " PDF files");
    this.pdfFile = this.pdfs[this.pdfIndex];
  }

  goToNextPage() {
    this.pageNumber = this.pageNumber + 1;
    console.log("Page " + this.pageNumber + " of " + this.numPages);

    if (this.pageNumber === this.numPages) {
      console.log("Reached end of current file, loading next pdf...");
      this.loadNextPdf();
    }
  }

  loadNextPdf() {
    this.pdfIndex += 1;

    if (this.pdfIndex === this.pdfs.length) {
      console.log("Reached end of pdf list, loading first pdf...");
      this.pdfIndex = 0;
    }

    this.pdfFile = this.pdfs[this.pdfIndex];
    this.pageNumber = 1;
    console.log(this.pdfFile);
  }

  tick() {
    this.setState(state => ({
      seconds: state.seconds + 1,
    }));
    
    if (this.state.seconds > 0 && (this.state.seconds % this.delay) === 0) {
      console.log("Loading next page...");
      this.goToNextPage();
    }
  }

  resetCounter() {
    this.setState(state => ({
      seconds: 0,
    }));
  }

  setNumPages({ pages }) {
    this.numPages = pages;
  }

  setDelay({ seconds }) {
    this.delay = seconds;
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }


  render() {

    var slideShow = this;
    const scale = 2.65;

    function onDocumentLoadSuccess({ numPages }) {
      slideShow.numPages = numPages;
      slideShow.setState(state => ({
        seconds: 0,
      }))
    }

    return (
      <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', backgroundColor: 'black', width: '1920px', height: '1080px'}}>
      <Document file={this.pdfFile} onLoadSuccess={onDocumentLoadSuccess}>
        <Page pageNumber={this.pageNumber} scale={scale}/>
      </Document>
    </div>
    );
  }
}


function App() {
  return (
      <Slideshow/>
  );
}

export default App;
