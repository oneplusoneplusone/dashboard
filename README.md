# Dashboard - React PDF Reader

This small React app allows you to display PDFs as a slideshow, in order to have an interactive dashboard for your company

# Requirements

- NodeJS >= 12.8.3
- NPM >= 6.14.6

# Usage

- Clone this repository
- From command-line:
```bash
npm install # Might take some time (~5mn on a Raspberry Pi 3)
npm start
```
- Your browser should automatically open on `localhost:3000` and display the pdf located at `src/default.pdf`
- In order to display other pdf files, just drop your pdfs in the `src/pdfs/` folder. The app should reload automatically as soon as there is a new file in folder.

# Tips

- You can replace the `default.pdf` file for your needs, it will also reload 
- By making the `src/pdfs/` folder visible on your network, you can easily set up a **_Drag & Drop_** dashboard for your company 

# Limitations

- It is not currently possible to build this app for production, as I was not able to handle the PDF-folder modifications properly (I'm thinking of using SSR to handle that part and optimize the response time)